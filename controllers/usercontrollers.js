/*
	Naming convention for controllers is that it should be named after the model/documents it is concerned with.
*/

//To create a controller, we first add it into our module.exports.
//So that we can import the controllers from our module.

//Controllers are function which contain the actual business logic of our API and is trigger by a route

//MVC - models, views and controller
//import the User model in the controllers instead because this is where we are now going to use it.
const User = require("../models/User");

//import Course
const Course = require("../models/Course")

const bcrypt = require('bcrypt');
//bcrupt is a package which allows us to has our passwords to add a layer of security for our user's details

//import auth.js module to use createAccessToken and its subsequent methods
const auth = require("../auth");

module.exports.registerUser = (req, res) => {

	//using bcrypt, we're going to hide the user's password underneath a layer of randomized characters. Salt rounds are the number of times we randomize the string/password hash
	//bcrypt.hashSync(<string>,<saltRounds>)
	const hashedPw = bcrypt.hashSync(req.body.password, 10);
	console.log(hashedPw);

	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPw,
		mobileNumber: req.body.mobileNumber
	});


	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))
};
//getUserDetails should only allow the LOGGED-in user to get his OWN details
module.exports.getUserDetails = (req, res) => {
	//console.log(req.user)
	//User.findOne({_id: req.body._id})
	//find() will return an array of documents which matches the criteria.
	//it will return an array even if it only found 1 document.
	//*User.find({id:req.body.id})

	//findOne() will return a single document that matches our criteria
	//User.findOne({_id:req.body.id})

	//findById() is a mongoose method that will allow us to find a document strictly by its id.

	//User.find({"_id": req.body._id})
	//This will allow us to ensure that the logged in user or the user that passed the token will be able to get his own details and only his own.

	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.loginUser = (req, res) => {

	console.log(req.body);

	/*
		Steps for logging in our user:

		1. find the user by its email
		2. if we found the user, we will check his password if the password input and the hashed password in our db matches.
		3. If we don't find a user, we will send a message to the client.
		4. If upon checking the found user's password is the same with out input password, we wiull generate a "key" for the user to have authorization to access certain features in our app.
	*/
	//mongodb: db.users.findOne({email:})
	User.findOne({email:req.body.email})
	.then(foundUser => {

		//foundUser is the parameter that contains the result of findOne
		//findOne() returns null if it is not able to match any document
		if(foundUser === null){
			return res.send({message: "No User found."})
		}else{
			//console.log(foundUser)
			//res.send(foundUser)
			//If we find a user, foundUser will contain the document that matched the input email.

			//Check if the input password from req.body matches the hashed password in our foundUser document
			/*
				bcrypt.compareSync(<inputString>, <hasedString>)

				If the inputString and the hashedString matches and are the same, the compareSync method will return true.
				else, it will return false.
			*/
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password);

			//console.log(isPasswordCorrect)
			//If the password is correct, we will create a "key", a token for our user, else, we will send a message
			if(isPasswordCorrect){

				/*
					To be able to create a "key" or token that allows/authorizes our logged in user around our application, we have to create our own module called auth.js

					This module will create an encoded string which contains our user's details

					This encoded string is what we call JSONWebtoken or JWT.

					This JWT can only be properly unwrapped or decoded with our own secret word/string.

				*/

				//console.log("We will create a token for the user if the password is correct")

				//auth.createAccessToken receives our foundUser document as an argument, gets only nesessary details, wraps those details in JWT and our secret, then finally returns our JWT. This JWT will be send to our client.
				return res.send({accessToken: auth.createAccessToken(foundUser)});

			}else{

				return res.send({message: "Incorrect password"})
			}
		}

	})

}

//checks if email exist or not
module.exports.checkUserEmail = (req, res) => {
	User.findOne({email:req.body.email})
	.then(result => {

		//findOne will result null if no match is found
		//send false if email does not exist
		//send true if email exist
		if(result === null){
			return res.send(false)
		}else {
			return res.send(true)
		}
	})

	.catch(error => res.send(error))
}

module.exports.enroll = async (req, res) => {

	//check the id of the user who will enroll?
	//console.log(req.user.id);

	//check the id of the course we want to enroll?
	//console.log(req.body.courseId);

	if(req.user.isAdmin){
		return res.send({message: "Action forbidden."})
	}

	/*
		Enrollment will come in 2 steps

		first, find the user who is enrolling and update his enrollments subdocument array.
		We will push the courseId in the enrollments array.
		Secondly, find the course where we are enrolling and update its enrollees subdocument array.
		We will push the userId in the enrollees array.

		Since we will access 2 collections in one action, we will have to wait for the completion of the action instead of letting javascript continue line per line.

		async and await - async keyword is added to a function to make our function asynchronous. Which means that instead of JS regular behavior of running each code line by line we will be able to wait for the result of a function.

		To be able to wait for the result of a function, we use the await keyword. The await keyword allows us to wait for the function to finish and get a result before proceeding

	*/

	//return a boolean to our isUserUpdated variable to determine the result of the query and if we were able to save the courseId into our user's enrollment subdocument array.

	let isUserUpdated = await User.findById(req.user.id).then(user =>{

		//check if you found the user's document:
		console.log(user)

		//Add the courseId in an object and push that object into the user's enrollments. 
		//Because we have to follow the schema of the enrillments subdocument array:

		let newEnrollment = {
			courseId: req.body.courseId
		}

		//access the enrollments array from our user and push the new enrollment subdocument into the enrollment array.
		user.enrollees.push(newEnrollment)

		//We must save the user document and return the value of saving our document.
		//then return true If we push the subdocument successfully
		//catch return error message if otherwise

		return user.save()
		.then(user => true)
		.catch(err => err.message)
	})

	//If user was able to enroll properly, isUserUpdated contains true.
	//Else, isUserUpdated will contain an error message
	//console.log(isUserUpdated);

	//Add an if statement and stop the process IF isUserUpdated Does not contain true
	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated});
	}

	//Find the course where we will enroll or add the user as an enrollee and return true If we were able to push the user into the enrollees array properly or send the error message instead.
	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {
		//console.log(course)
		//contains the found course or the course we want to enroll in

		//create an object to pushed into the subdocument array, enrollees.
		//We have to follow the schema of our subdocument array
		let enrollee = {
			userId: req.user.id
		}

		//push the enrollee into the enrollees subdocument array of the course:
		course.enrollees.push(enrollee);

		//save the course document
		//return true if we were able to save and add the user as enrollee properly
		//return an err message if we catch an error.
		return course.save().then(course => true).catch(err => err.message)

	})

	console.log(isCourseUpdated)

	//If isCourseUpdated is not true, send the error message to the client and stop the process
	if(isUserUpdated !== true){
		return res.send({message: isCourseUpdated})
	}

	//Ensure that we were able to both update the user and course document to add our enrollment and enrollee respectively and send a message to the client to end the enrollment process.

	if(isUserUpdated && isCourseUpdated){
		return res.send({message: "Thank you for enrolling!"})
	}

}
