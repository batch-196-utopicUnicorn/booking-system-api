
const express = require('express');
const router = express.Router();


const userControllers = require('../controllers/userControllers')


//Controllers are function which contain the actual business logic of our API and is trigger by a route

//console.log(userControllers)

//import auth to be able to have access and use the verify methods to act as middleware for our routes
//Middlewares add in the route such as verify() will have access to the req, res objects.

const auth = require("../auth");

//destructure auth to get only our methods and save it in variables:

const {verify} = auth;
console.log(verify)
/*
	Updated Route Syntax:
	router.method("/endpoint", handlerFunction)
*/

//register
router.post('/',userControllers.registerUser)

//In fact, routes should just contain the endpoints and it should only trigger the function but it should not be where we define the logic of our function.

//The business logic of our API should be in controllers..
//verify() is used as a middleware which means our request will get through verify first before our controller
router.get("/details",verify, userControllers.getUserDetails )


//Route for User Authentication
router.post('/login', userControllers.loginUser);

//Check Email
router.post('/checkEmail', userControllers.checkUserEmail);

//User Enrollment
//courseId will come from the req.body
//userId will come from the req.user
router.post('/enroll', verify, userControllers.enroll);

module.exports = router;
